<?php

class Product
{
    private $conn;
    private $table_name = "products";

    public $productid;
    public $name;
    public $description;
    public $price;
    public $categoryid;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}
?>