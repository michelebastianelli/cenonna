<?php

class Database
{
    private $host = "localhost";
    private $name = "cenonna";
    private $user = "root";
    private $password = "";

    public $conn;

    public function getConnection() {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->name, $this->user, $this->password);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $this->conn;
    }
}
?>