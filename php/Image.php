<?php

class Image
{
    private $conn;
    private $table_name = "images";

    public $imageid;
    public $productid;
    public $path;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}
?>