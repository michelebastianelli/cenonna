<?php

class Category
{
    private $conn;
    private $table_name = "categories";

    public $categoryid;
    public $name;
    public $description;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}
?>